+++
author = "Julia Browne"
date = 2020-05-01
title = "Visualizing Ice Cream Flavours"
series = "how-to guides"
css = "style.css"
+++

**Note:** The content of this page has been heavily altered to redact confidential work information. :icecream:

---

This page describes how to use the Rainbow Sprinkles (RS) User Interface (UI) 
to visualize flavour results from the Ice Cream Service.

# Visualizing Flavour Results

You can visualize ice cream flavour results as:

- A table
- A line graph
- A distribution graph
- A CSV file
- A PNG image
- A PDF file

To visualize flavour results, perform the following steps.

1. Configure your ice cream data to enable flavour results graphs.
2. Display results in a table, a line graph, or a distribution graph.
3. Filter results using one of the available types of filters.
4. Select a flavour from the side panel to view details about a specific flavour.

# Configuring Flavour Results

Flavour configurations use Rainbow Sprinkles Language (RSL) and are defined as RSL Scripts.

## Setting Up RSL Scripts

To enable flavour graphs in the RS UI, you must specify the following information in the flavour statement of your RSL scripts.

|Parameters               |Description                                                                                                                     |
|:--------------------------|:---------------------------------------------------------------------------------------------------------------------------------|
| `milkContent = (sugarContent)` | Cross-references milk content with sugar content.                                                                          |
| `churnTime = (ingredientCount)`  | Cross-references churning time with ingredient count.                                                                          |
| `milk = (milkQuality)`  | (Optional) Displays quality of milk used.                                                                                    |
| `creamyTexture = (textures)`  | (Optional) Displays creaminess level of ice cream. |

### Example

See the following code sample for an RSL script configured to enable flavour graphs.

```
// Set the milkQuality and churnTime attributes for the UI
val results = where(milk IS "2%") -> 
	setAttribute(milkQuality, "High") ->
	setAttribute(ingredientCount, milk & sugarContent)

// Export texture quality and flavour files
flavour(results,
        name="flavourContext",
        milk=(ingredientCount, milk, milkQuality),
        churnTime=(ingredientCount, creamyTexture))
```
# Displaying Flavour Results

Use one of the following methods to display the Flavour Results page:

- Navigate to the **Ice Cream** page and click **View Flavours**.
- Navigate to configuration **History** and click **View Flavours**.
- Select a **Flavour Configuration** and click the **Latest Results** icon in the configuration page.

The system displays ice cream flavours in table format by default.

To display the data as a line graph, click the Line icon on the top right-hand corner of the Flavour Results page. 

To display the data as a distribution graph, click the Distribution icon in the top right-hand corner of the Flavour Results page.

With the graphs, you can:

- Move the cursor over the graph to display available flavours.
- Move the cursor over the flavour labels listed under the x-axis to highlight flavour details.
- Left-click a flavour label to remove the flavour from the graph.
- Double-click a flavour label to isolate an individual flavour.
- Highlight an area of the graph to zoom in on a particular flavour.

## Selecting Flavour Results

The system can generate multiple flavour results files for each job. You can choose which files to display by selecting results.

Flavour file results provide taste and variety data, while texture file results provide ingredient quality data. 
Use the **Flavour/Texture toggle** to switch between flavour and texture file results.

To select from multiple flavour results, click the **drop-down arrow**. When you switch between results files, the results filters reset.

## Downloading Flavour Results

You can download flavour results in one of the following ways:
- To generate a CSV file, click **Download**.
- To generate a PNG image or a PDF file, click the **menu icon** and select from the available options.

## Sharing Flavour Results

Click **Copy Link** to copy the URL link of the results to your clipboard. The link includes active filters and allows you to share flavour 
results with other ice cream fans.

# Filtering Flavour Results

Use the UI results filter to filter the results displayed in tables and graphs. You can do so by using the following types of filters:
- Chocolate flavour results
- Fruit flavour results
- Custom flavour results

The flavour results filters reset when you switch between results files.

To use the chocolate flavour filter, select one of hazelnut, dark chocolate, or white chocolate. 
The tables and graphs update automatically after a filter is selected.

To use the fruit flavour filter, click the **drop-down arrow** on the upper left-hand side of the screen and select a fruit from the list.

To use the custom flavour filter, click **+ Add Filter** and enter the desired filter. 
You can select from existing filters, or enter a string to specify a new filter.

## Configuring the Custom Flavour Results Filter

You can define and apply a filter to selected ice cream flavours. These flavours must be specified in your RSL script.

- To use existing filters, click **+Add Filter**.
- Insert a comma-separated list for each filter by adding an **OR** condition to a flavour. 
For example, when using `flavour = vanilla.pistachio.mango, vanilla.pistachio.strawberry` both sets of factors are selected.
- Click the **X** character to the right of the filter to remove it.

Additionally, you can filter by:

- The beginning of a string. For example, `flavour = vanilla.pistachio.mango`.
- The end of a string. For example, `milkQuality = High`.
- A sub-string. For example, `milk = 2%`.
- Any combination of the above. For example, `ingredientCount = vanilla.pistachio.mango*High*2%`.



























