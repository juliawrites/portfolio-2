+++
author = "Julia Browne"
date = 2020-05-05
title = "Calculating Coffee Shop Sales"
series = "how-to guides"
css = "style.css"
+++

**Note:** The content of this page has been heavily altered to redact confidential work information. :coffee:

---

# Prerequisites
To calculate all coffee shop sales, you must first create a pricing environment.

# Instructions
After you have created a pricing environment, submit the `POST` request:

```
POST <baseUrl>/coffeeShopEnv/{coffeeShopEnvId}/coffeeSales
```

The details of this request depend on:

- The coffee shop sales scheme you want to calculate.
- The currency you are using.
- Any optional parameters relevant to the coffee shop sales you want to calculate. Optional parameters include the location of the shop and the variety of the menu.

**_Note:_** You can omit the output parameter in your request, wait until the coffee shop sales calculation finishes, and then 
request a report separately. This report provides more control over the data you might want to export.

The response to the `POST` request includes the `coffeeSalesId` used to poll for the status of the request. Submit the `GET` request:

```
GET <baseUrl>/coffeeShopEnv/{coffeeShopEnvId}/coffeeSales/{coffeeSalesId}
```

When the status changes to `Completed`, the calculation is complete. The requested coffee shop sales have been computed and are available for reporting.

## Calculating Cappuccino Sales
To calculate cappuccino sales, submit a `POST` request similar to the following example:

```    
{    
    "label": "Calculating Cappuccino Sales",
    "coffeeShopLocation": "data/CoffeeShopLocation/Location.csv",
    "coffeeBeanType": "data/CoffeeBeanType/DarkRoast.csv",
    "milkType": "3.25%",
    "cappuccinoData": {
        "extraFoam": "data/CapuccinoData/ExtraFoam.csv",
        "doubleShot": "data/CapuccinoData/DoubleShot.csv",
        "vanillaSyrup": "data/CapuccinoData/VanillaSyrup.csv"
    }
}
```

## Calculating Iced Tea Sales

To calculate iced tea sales, submit a `POST` request similar to the following example:

```
{
    "label": "Calculating Iced Tea Sales",
    "coffeeShopLocation": "data/CoffeeShopLocation/Location.csv",
    "icedTeaType": "data/IcedTeaType/GreenTea.csv",
    "iceQuantity": "25%",
    "icedTeaData": {
        "extraIce": "data/IcedTeaData/ExtraIce.csv",
        "addLemonade": "data/IcedTeaData/AddLemonade.csv",
        "blackTea": "data/IcedTeaData/BlackTea.csv"
    }
}
```

# Output

The output depends on the optional parameters specified in the `POST` request.

# Next Steps

After calculating all coffee shop sales, you can:

- Validate your coffee sales by exporting your results. 
- Share your results with other employees.
- Generate a more detailed report by calculating data from different drinks sold at the coffee shop.
