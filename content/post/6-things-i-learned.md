+++
author = "Julia Browne"
date = 2020-05-10
title = "6 Things I Learned as a New Technical Writer"
series = "blog posts"
css = "style.css"
+++

Click [here](https://medium.com/@juliawritestech/6-things-i-learned-as-a-new-technical-writer-dc62e5a589eb) 
for the original Medium article.

---

Last year I decided to take the plunge and apply for a technical writing job. 
I had been working as a software developer for almost a year, and coding was fun, sure! 
But it was also stressful, difficult, and just… not for me.

I figured that out pretty early on in my dev job, and spent many nights awake in bed, 
staring at the ceiling, wondering what the hell I was doing with my life. 
On a chilly October weekend, coming home from a trip to Whole Foods, I turned to my boyfriend and said, 
"What if I became a technical writer?" He smiled at me and replied, "That's the best idea you've ever had."
So I spent that evening rewriting my resume and cover letter while he put our groceries away. 
I wrote my first "how to use an API" guide (laughingly bad now that I look back at it), 
and sent something like eight or nine job applications out into the world.

A month later I found myself sitting at my new desk, in my new office, doing work that didn't feel like work. 
Gone are the days of restless sleep; now I wake up extra early and make my way to work with a croissant 
in my hand and a grin on my face.

Making this career change is the best thing I’ve ever done. I love technical writing and can’t believe I get to do it for a living. 
I mean, I’m getting paid to write words! What more could I have ever asked for?!

![typing](/../images/typing1.gif)

If you're also looking to make the switch to tech writing, I have some advice. 
Here's a list of things I learned during my first six months at the job:

## 1. You don't know as much as you think you know

Technical writing is very different from writing Medium articles or Harry Potter fanfiction (trust me, I speak from experience). 
It involves being as clear and concise as possible, writing in a non-personal style, and knowing what you're talking about. 
Your aim isn't to entertain your reader, but to provide them with information.

my pride aside and admit that, while I could write, I couldn't write technically. But I learned. 
I absorbed as much knowledge as I could from my co-workers, books on writing, and the internet. 
I kept a file on my work computer with links to resources and instructions on writing and publishing documentation. 
I still refer to it once in a while, despite feeling much more confident in my work now.

## 2. Harsh feedback sucks but you should listen to it

![feedback](/../images/feedback1.png)

My first week at my job, a senior writer reviewed a page I'd written and sent me a very long email pointing out 
issues with my formatting and admonishing me for using too many unnecessary words. My heart kind of sank into my stomach, 
and I started questioning whether I could do the job at all. If I couldn't write perfectly from the start, 
was there even any hope for me?

Turns out that yes, there is. There was hope for me and there will be hope for you, too. 
I'm a much better writer now than I was at the beginning thanks to the feedback of my more experienced co-workers. 
I welcome feedback now, good or bad, and have learned to see it as an opportunity to improve instead of a personal attack.

I have also learned that you need to let yourself feel upset. Harsh feedback hurts: it feels personal 
and makes you doubt your abilities. Take a step back from your work, take a deep breath, take a sip of your coffee.

Then take that feedback and apply it.

## 3. Keep your audience in mind, always

A project I took on at my work was creating audience profiles for one of our products. 
There are many people who read our documentation, and they all have different levels of knowledge, 
experience, and training depending on their role. Breaking down our audience and creating personas for each reader 
helped my team and I tailor our writing to their needs and expectations.

For example, you'll want to use terminology your audience is familiar with. 
If they're not familiar with it, you'll want to explain it. 
There's a fine line between over-explaining and under-explaining concepts, 
but if you learn to walk it your docs will improve and your readers will be grateful.

Knowing your audience will also help you organize your documentation better!

## 4. Watch out for the elusive scope creep

![scopecreep](/../images/scopecreep.gif)

As you write, don't forget what the goal of your task is. It's very easy to fall for scope creep, 
or have your task's requirements increase over its lifecycle. Sometimes what started as a small change 
to an existing page exposes a gap in the documentation and turns into you writing a whole new page, 
and a half hour job grows into a multiple-day one.

I like the way my manager handles scope creeps. She has a large whiteboard by her desk, 
and when the scope of one of our tasks changes she pulls out her dry erase markers and breaks 
the new requirements down. She takes into account how they fit into our current sprint, 
which documentation release they'll belong to, and how much bandwidth we have to handle them.

## 5. Choose a style guide and stick with it

My manager has drilled into my head that consistency is key in what we do. 
To achieve that, we refer to a style guide (we use the Microsoft Manual of Style, but there are many others out there). 
I always have a copy of it open in my second monitor; if I'm working on a page and need to know what to call a certain UI element, 
all I have to do is check the style guide.

## 6. You become a better writer by… writing

It's silly but it's true: the more you write the better you become at it.

If you want to write more, take a look at things like 
[open source projects](https://amrutaranade.com/2018/03/21/list-of-open-source-projects-that-accept-docs-contributions/)  - 
they're always looking for extra help!
