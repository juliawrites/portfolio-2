+++
title = "resume"
date = "2020-05-01"
css = "style.css"
+++

Click [here](https://drive.google.com/file/d/1P15ItU2Kl3z6BDuVgGGDT6r3md_j9rJZ/view?usp=sharing) 
for a PDF version.

---

## Skills :sparkles:

- Markdown
- Confluence
- Adobe FrameMaker
- Javascript
- HTML & CSS

## Experience :coffee:

### Technical Writer | IHS Markit | Nov 2019 - Present

I write, edit, and maintain API and end-user documentation for the Financial Risk Analytics division.

I am responsible for:

- Publishing bi-weekly release notes
- Documenting internal processes
- Creating documentation templates

### Software Developer | Keela | Mar 2019 - Nov 2019

I built and maintained front and back-end software for a CRM for non-profits.
I also implemented the company's first documentation process.

### Editor-In-Chief | LOGOS Journal | Sept 2017 - June 2018

I managed a team of seven editors through the selection, editing, and publishing of 
student papers for UBC's Classical, Near Eastern & Religious Studies Student Journal.

### Field Archaeologist | The Gerace Project | May 2016 - June 2016

I took part in an archaeological dig in sunny Gerace, Sicily. I helped excavate Roman structures and
establish the chronology and nature of post-Roman occupation of the area.

## Education :books:

### Simon Fraser University | Technical Communication

### University of British Columbia | History & Classical Studies

### RED Academy | Web & App Development
