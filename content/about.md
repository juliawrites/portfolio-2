+++
title = "about me"
date = "2020-05-01"
css = "style.css"
+++

Hi, I’m Julia. I’m a technical writer. :pencil2:

As a kid I had my nose constantly buried in a book, 
and as I got older I began writing my own stories. 
I’ve dabbled in many different genres and styles, 
and in the past year managed to achieve my childhood dream 
of turning writing into a career.

I might not be creating fantasy worlds and weaving wordy, 
adverb-filled stories, but I find technical writing just as exciting as writing fiction. 
Documenting software has taught me the value of simplicity, 
and left me in awe at how often few words can convey so much of knowledge. 
In certain ways, it’s poetry to me.

My transition from creative writer to technical writer 
was a fairly smooth one; any bumps on the road were made flat 
by how determined I was to make a career out of this. 
Before becoming a tech writer, I worked as a software developer at a startup. 
I’ve also been a part-time barista, a field archaeologist, and a chocolate factory manager. 
All of these jobs were satisfying in their own way, but none of them fulfilled me. 
I guess I didn’t feel complete until I was finally reunited with my first love: words.

When I’m not writing I enjoy watching horror movies and playing video games. 
I’ve watched the entirety of The Office more times than I can count, 
and listened to so many hours of true crime podcasts that Spotify doesn’t know what to recommend me anymore. 
I live in a tiny apartment in downtown Vancouver with my partner and our two cats, 
and on the weekends I drink a lot of coffee and take a lot of naps.

I’ve included some of my writing [here](https://juliawrites.tech/post/); feel free to take a look if you’d like!
