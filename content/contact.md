+++
title = "get in touch"
date = "2020-05-01"
css = "style.css"
+++

If you’d like to hire me, inquire about me, 
or chat about cats over coffee, you can:

- send me an [email](mailto:julia.browne@live.ca?subject=Hi%2C%20Julia!), or
- reach out to me on [LinkedIn](https://www.linkedin.com/in/ju-browne/)!
